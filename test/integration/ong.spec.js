const app = require('../../src/app')
const request = require('supertest')
const connection = require('../../src/database/connection')

const ong_test_data = {
	name: "Teste",
	email: "lool@gmail.com",
	whatsapp: "551160389291",
	city: "São Paulo",
  uf: "SP",
}

describe('ONG', () => {
  beforeEach(async () => {
    await connection.migrate.rollback()
    await connection.migrate.latest()
  })

  afterAll(async () => {
    await connection.destroy()
  })

  it('should be able to create a new ONG', async () => {
    const response = await request(app)
      .post('/ongs')
      .send(ong_test_data)

    expect(response.body.id).toHaveLength(10)
  })

  it('should be able to check all the ONGs', async () => {
    await request(app).get('/ongs').expect(200)
  })
})