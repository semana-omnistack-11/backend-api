const app = require('../../src/app')
const request = require('supertest')
const connection = require('../../src/database/connection')

let ong

const ong_test_data = {
	name: "Teste",
	email: "lool@gmail.com",
	whatsapp: "551160389291",
	city: "São Paulo",
  uf: "SP",
}

const case_test_data = {
	title: `Uma causa muito importante`,
	description: `Descrevendo o problema para que você nos ajude`,
	value: 30
}

describe('Case', () => {
  beforeAll(async () => {
    // await connection.migrate.rollback()
    await connection.migrate.latest()

    // creating an instance to show up
    ong = await request(app)
      .post('/ongs')
      .send(ong_test_data)
  })

  afterEach(async () => {
    await connection('cases').del()
  })

  afterAll(async () => {
    await connection.destroy()
  })

  it('should be able to create a new Case', async () => {
    await request(app).post('/cases')
      .set('Authorization', ong.body.id)
      .send(case_test_data).expect(200)
  })

  it('sould be able to check all cases', async () => {
    await request(app).get('/cases').expect(200)
  })

  it('sould show an specific case', async () => {
    const { body } = await request(app).post('/cases')
      .set('Authorization', ong.body.id)
      .send(case_test_data)

    await request(app).get(`/cases/${body.id}`).expect(200)
  })

  it('sould delete an specific case', async () => {
    const { body } = await request(app).post('/cases')
      .set('Authorization', ong.body.id)
      .send(case_test_data)

    await request(app).delete(`/cases/${body.id}`)
     .set('Authorization', ong.body.id).expect(202)
  })

})