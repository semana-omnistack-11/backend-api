// Update with your config settings.

module.exports = {

  development: {
    client: 'sqlite',
    connection: {
      filename: './src/database/dev.sqlite'
    },
    migrations: {
      directory: './src/database/migrations'
    },
    useNullAsDefault: true
  },

  test: {
    client: 'sqlite',
    migrations: {
      directory: './src/database/migrations'
    },
    connection: {
      filename: './src/database/test.sqlite'
    },
    useNullAsDefault: true
  }

};
