const connection =  require('../database/connection')

module.exports = {
  async index(request, response) {
    const ong = request.headers.authorization
    const cases = await connection('cases').where('ong_id', ong)

    return response.json(cases)
  }
}