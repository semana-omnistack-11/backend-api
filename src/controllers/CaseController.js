const connection =  require('../database/connection')

module.exports = {
  async create(request, response) {
    const { title, description, value } = request.body
    const ong_id = request.headers.authorization
  
    const [inserted] = await connection('cases').insert({
      title, description, value, ong_id
    })
  
    return response.json({ id: inserted })
  },

  async all(request, response) {
    const { page = 1 } = request.query

    const [count] = await connection('cases').count() 
    const cases = await connection('cases')
      .join('ongs', 'ongs.id', '=', 'cases.ong_id')
      .limit(5).offset((page - 1) * 5).select(
        "cases.*",
        "ongs.name",
        "ongs.email",
        "ongs.whatsapp",
        "ongs.city",
        "ongs.uf",
      )
    
    response.header('X-Total-Count', count['count(*)'])
    return response.json(cases)
  },

  async show(request, response) {
    const { id } = request.params
    const cause = await connection('cases').where('id', id).select().first()

    return response.json(cause)
  },

  async delete(request, response) {
    const { id } = request.params
    const ong = request.headers.authorization || ''

    const cause = await connection('cases')
      .where({
        'id': id,
        'ong_id': ong
      }).select().first()


    if(!!cause) {
      await connection('cases').where('id', id).delete()
      return response.status(202).json(
        {message: `Case #${id} has been deleted`}
      )
    } else {
      return response.status(401).json(
        { error: 'This case does not exist or you are not allowed to delete it'}
      )
    }
  }
}