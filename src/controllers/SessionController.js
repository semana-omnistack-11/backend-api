const connection =  require('../database/connection')
const crypto = require('crypto')

module.exports = {
  async create(request, response) {
    const { ong_id } = request.body

    const ong = await connection('ongs')
      .where('id', ong_id)
      .select().first()

    if(!ong) {
      return response.status(404).json()
    }

    return response.json(ong)
  }
}