const knex = require('knex')
const configs = require('../../knexfile')

const connection = process.env.NODE_ENV === 'test' ? knex(configs.test) : knex(configs.development)

module.exports = connection