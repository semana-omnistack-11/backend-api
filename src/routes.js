const express = require('express')
const OngController = require('./controllers/OngController')
const CaseController = require('./controllers/CaseController')
const ProfileController = require('./controllers/ProfileController')
const SessionController = require('./controllers/SessionController')
const { celebrate, Segments, Joi } = require('celebrate')

const routes = express.Router()
const auth_verify = {
  [Segments.HEADERS]: Joi.object({
    authorization: Joi.string().required()
  }).unknown()
}

/**
 * Para acessar parâmetros
 * 
 * de query --> request.query
 * de rotas --> request.params
 * de requisição --> request.body
 */

routes.get('/ongs', OngController.all)

routes.post('/ongs', celebrate({
  [Segments.BODY]: Joi.object().keys({
    name: Joi.string().required(),
    email: Joi.string().required().email(),
    whatsapp: Joi.string().required().min(12).max(13),
    city: Joi.string().required(),
    uf: Joi.string().required().length(2)
  })
}), OngController.create)

routes.get('/cases', celebrate({
  [Segments.QUERY]: Joi.object().keys({
    page: Joi.number()
  })
}), CaseController.all)

routes.get('/cases/:id', CaseController.show)

routes.post('/cases', celebrate({
  [Segments.BODY]: Joi.object().keys({
    title: Joi.string().required(),
    description: Joi.string().required(),
    value: Joi.number().required().min(1),
  })
}), CaseController.create)

routes.delete('/cases/:id', celebrate(auth_verify), celebrate({
  [Segments.PARAMS]: Joi.object().keys({
    id: Joi.number().required()
  })
}), CaseController.delete)

routes.get('/profile', celebrate(auth_verify), ProfileController.index)

routes.post('/session', celebrate({
  [Segments.BODY]: Joi.object().keys({
    ong_id: Joi.string().required()
  })
}), SessionController.create)

module.exports = routes