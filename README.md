# Backend API

## Quick Notes on Deploying your app

When you're doing your app,and you wanna brag about it with your friends or just show it up, the best thing is to deploy it to Herokku. Yet it is a powerfull plataform, it is a little expensive, too.

But now, let's say it became a little more serious, and your app now has some comercial purposes. Heroku now may not be the best of the options. As said in class, Digital Ocean may be better, couse its pricification it's quite predictable, and even the cheapest options can be enough for it

But if it goes viral, maybe Google Cloud Service, Microsoft Azurre ou Amazon Web Service, may be better options.

This advise is good for the backend. As for the frontend, when the application meets the first ou second scenario, Netlify will be nice and easy to use, but it can become expensive like Heroku if it scales. For the last scenario, GCS, Azurre or AWS can do the magic.